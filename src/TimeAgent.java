import jade.core.Agent;
import jade.core.behaviours.WakerBehaviour;

public class TimeAgent extends Agent {
    protected void setup() {
        // Printout a welcome message
        System.out.println("SALU TWA "+getAID().getName()+" SAVA?");

        addBehaviour(new WakerBehaviour(this, 10000) {
            protected void handleElapsedTimeout() {

            }
        } );
    }
}
